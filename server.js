// MEAN Stack RESTful API Tutorial - Contact List App

var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db1 = mongojs('TT', ['servicelist']);
var bodyParser = require('body-parser');
var db2 = mongojs('TT', ['ussdlist']);
var db3=mongojs('TT', ['offrelist']);
app.use(express.static(__dirname + '/../app'));
app.use(bodyParser.json());

app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'Content-Type');
  next();
});


app.get('/servicelist', function (req, res) {
  console.log('I received a GET request');

  db1.servicelist.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});
app.get('/offrelist', function (req, res) {
  console.log('I received a GET request');

  db3.servicelist.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/servicelist', function (req, res) {
  console.log(req.body);
  db1.servicelist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
  db2.ussdlist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});
app.post('/offrelist', function (req, res) {
  console.log(req.body);
  db3.offrelist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
  db2.ussdlist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/servicelist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db1.servicelist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
  db2.ussdlist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});
app.delete('/offrelist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db3.offrelist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
  db2.ussdlist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/servicelist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db1.servicelist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });

});
app.get('/offrelist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db3.offrelist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });

});
app.put('/servicelist/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.nom);
  db1.servicelist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {nom: req.body.nom, description: req.body.description, code: req.body.code , status:req.body.status}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
  db2.ussdlist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {nom: req.body.nom, description: req.body.description, code: req.body.code , status:req.body.status}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});
app.put('/offrelist/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.nom);
  db3.offrelist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {nom: req.body.nom, description: req.body.description, code: req.body.code , status:req.body.status}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
  db2.ussdlist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {nom: req.body.nom, description: req.body.description, code: req.body.code , status:req.body.status}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.listen(3000);
console.log("Server running on port 3000");